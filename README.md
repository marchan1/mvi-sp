 # The task
 ## Time series modelling
Select at least two appropriate time series datasets and compare the following three types of Neural Networks:
 + convolutional neural networks 
 + basic recurrent neural networks
 + gated recurrent neural networks (Long Short-Term Memory (LSTM) or Gated Recurrent Unit (GRU))
 
Both selected time series are placed at the root.
Downloaded from:
https://datamarket.com/data/list/?q=provider%3Atsdl

1st series - https://datamarket.com/data/set/22ox/monthly-milk-production-pounds-per-cow-jan-62-dec-75#!ds=22ox&display=line

2nd series - https://datamarket.com/data/set/22qx/monthly-australian-imports-from-japan-thousands-of-dollars-jul-65-oct-93#!ds=22qx&display=line